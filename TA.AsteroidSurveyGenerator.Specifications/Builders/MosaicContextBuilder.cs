﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: MosaicContextBuilder.cs  Last modified: 2018-07-12@20:07 by Tim Long

using System;
using TA.AsteroidSurveyGenerator.Library;
using TA.AsteroidSurveyGenerator.Specifications.TestHelpers;
using TA.ObjectOrientedAstronomy.FundamentalTypes;

namespace TA.AsteroidSurveyGenerator.Specifications.Builders
    {
    class MosaicContextBuilder
        {
        UnitTestSensor sensor = new UnitTestSensor(0, 0, 0, 0);
        readonly MosaicContext context = new MosaicContext();
        MosaicGeneratorStrategy generatorStrategy = MosaicGeneratorStrategy.Box;
        double overlapFraction;

        internal MosaicContext Build()
            {
            context.Sensor = sensor;
            switch (generatorStrategy)
                {
                    case MosaicGeneratorStrategy.RowMajor:
                        context.Generator = new RowMajorMosaicGridGenerator(sensor, overlapFraction);
                        break;
                    default:
                        throw new NotSupportedException(generatorStrategy.ToString());
                }
            return context;
            }

        public MosaicContextBuilder WithSensor(int xPixels, int yPixels, double xScale, double yScale)
            {
            sensor = new UnitTestSensor(xPixels, yPixels, xScale, yScale);
            return this;
            }

        public MosaicContextBuilder WithOneDegreeSquareSensor()
            {
            return WithSensor(3600, 3600, 1.0, 1.0);
            }

        public MosaicContextBuilder StartAt(double hourAngle, double declination)
            {
            context.StartTile = new MosaicTile(new RightAscension(hourAngle), new Declination(declination));
            return this;
            }

        public MosaicContextBuilder TerminateAt(double hourAngle, double declination)
            {
            context.Terminator = new MosaicTile(new RightAscension(hourAngle), new Declination(declination));
            return this;
            }

        public MosaicContextBuilder InRowMajorOrder()
            {
            generatorStrategy = MosaicGeneratorStrategy.RowMajor;
            return this;
            }

        public MosaicContextBuilder OverlapFraction(double overlapFraction)
            {
            this.overlapFraction = overlapFraction;
            return this;
            }
        }
    }