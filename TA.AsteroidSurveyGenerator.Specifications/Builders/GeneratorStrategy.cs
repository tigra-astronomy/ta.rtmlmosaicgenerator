﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: GeneratorStrategy.cs  Last modified: 2018-08-09@19:13 by Tim Long

namespace TA.AsteroidSurveyGenerator.Specifications.Builders
    {
    enum GeneratorStrategy
        {
        SingleImage,
        AsteroidSeries
        }
    }