﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: MosaicContext.cs  Last modified: 2018-07-12@19:59 by Tim Long

using TA.AsteroidSurveyGenerator.Library;

namespace TA.AsteroidSurveyGenerator.Specifications.Builders
    {
    class MosaicContext
        {
        public IMosaicGenerator Generator { get; set; }

        public IImagingSensor Sensor { get; set; }

        public IEquatorialCoordinate StartTile { get; set; }

        public IEquatorialCoordinate Terminator { get; set; }
        }
    }