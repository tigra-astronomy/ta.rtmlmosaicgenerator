﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: RtmlGeneratorContext.cs  Last modified: 2018-08-09@22:46 by Tim Long

using System.Collections.Generic;
using System.Xml.Linq;
using TA.AsteroidSurveyGenerator.Library;

namespace TA.AsteroidSurveyGenerator.Specifications.Builders
    {
    class RtmlGeneratorContext
        {
        public IGenerateRtml Generator { get; set; }

        public IEnumerable<MosaicTile> Targets { get; set; } = new List<MosaicTile>();

        public SingleImageRtmlGenerator SingleImageRtmlGenerator =>
            Generator as SingleImageRtmlGenerator;

        public XElement ExpectedXml { get; set; }

        public ImageSpecification ImageSpecification { get; set; }

        public RtmlContact RtmlContact { get; set; }

        public RtmlRequest RtmlRequest { get; set; }
        }
    }