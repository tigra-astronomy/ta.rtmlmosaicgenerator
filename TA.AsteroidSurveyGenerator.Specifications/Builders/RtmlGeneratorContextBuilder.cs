﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: RtmlGeneratorContextBuilder.cs  Last modified: 2018-08-09@23:56 by Tim Long

using System;
using System.Xml.Linq;
using TA.AsteroidSurveyGenerator.Library;
using TA.AsteroidSurveyGenerator.Specifications.TestHelpers;

namespace TA.AsteroidSurveyGenerator.Specifications.Builders
    {
    class RtmlGeneratorContextBuilder
        {
        XElement expectedXml;
        ImageSpecification imageSpecification = new ImageSpecification();
        GeneratorStrategy generatorStrategy = GeneratorStrategy.SingleImage;
        RtmlGeneratorContext context;
        readonly RtmlContact contact = DefaultContact;
        readonly RtmlRequest project = DefaultProject;

        public RtmlGeneratorContext Build()
            {
            context = new RtmlGeneratorContext();
            context.RtmlContact = contact ?? DefaultContact;
            context.RtmlRequest = project ?? DefaultProject;
            context.ExpectedXml = expectedXml;
            context.ImageSpecification = imageSpecification;
            context.Generator = SelectRtmlGeneratorStrategy();

            return context;
            }

        IGenerateRtml SelectRtmlGeneratorStrategy()
            {
            switch (generatorStrategy)
                {
                    case GeneratorStrategy.SingleImage:
                        return new SingleImageRtmlGenerator(context.ImageSpecification, context.RtmlContact,
                            context.RtmlRequest);
                    case GeneratorStrategy.AsteroidSeries:
                        return new AsteroidSeriesRtmlGenerator();
                }
            throw new InvalidOperationException("An undefined RTML generation strategy was specified.");
            }

        public RtmlGeneratorContextBuilder OutputShouldBeLike(string resourceName)
            {
            var xmlSpecimenData = TestData.FromEmbeddedResource(resourceName);
            expectedXml = XElement.Parse(xmlSpecimenData);
            return this;
            }

        public RtmlGeneratorContextBuilder ImageSpecification(double exposureTime, int binning, string filterName,
            int count,
            string description)
            {
            imageSpecification = new ImageSpecification
                {
                Binning = binning,
                Exposure = exposureTime,
                Name = filterName,
                Description = description,
                FilterName = filterName,
                Count = count
                };
            return this;
            }

        public RtmlGeneratorContextBuilder RtmlGenerationStrategy(GeneratorStrategy strategy)
            {
            generatorStrategy = strategy;
            return this;
            }

        static RtmlContact DefaultContact => new RtmlContact
            {
            User = "Unit Tester",
            Email = "UnitTester@nowhere.org",
            Organization = "Tigra Astronomy"
            };

        static RtmlRequest DefaultProject => new RtmlRequest
            {
            Project = "Test Project",
            PlanName = "1"
            };
        }
    }