﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: ShouldExtensions.cs  Last modified: 2018-07-12@20:07 by Tim Long

using System.Collections.Generic;
using System.Linq;
using Machine.Specifications;
using TA.AsteroidSurveyGenerator.Library;

namespace TA.AsteroidSurveyGenerator.Specifications.TestHelpers
    {
    static class ShouldExtensions
        {
        public static void ShouldBeTheSameTargetCoordinatesAs(this IEnumerable<MosaicTile> actual,
            IEnumerable<MosaicTile> expected)
            {
            var actualList = actual.ToList();
            var actualCount = actualList.Count;
            var expectedList = expected.ToList();
            var expectedCount = expectedList.Count;
            if (actualCount != expectedCount)
                throw new SpecificationException(
                    $"Collections are different sizes, expected {expectedCount}, actual {actualCount}");
            for (var i = 0; i < actualCount; i++)
                {
                var expectedCoordinate = expectedList[i];
                var actualCoordinate = actualList[i];
                expectedCoordinate.RightAscension.Value.ShouldBeCloseTo(actualCoordinate.RightAscension.Value);
                expectedCoordinate.Declination.Value.ShouldBeCloseTo(actualCoordinate.Declination.Value);
                }
            }
        }
    }