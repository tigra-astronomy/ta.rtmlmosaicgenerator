﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: TestData.cs  Last modified: 2018-08-08@17:59 by Tim Long

using System.IO;
using System.Reflection;

namespace TA.AsteroidSurveyGenerator.Specifications.TestHelpers
    {
    static class TestData
        {
        internal static string FromEmbeddedResource(string resourceName)
            {
            var asm = Assembly.GetExecutingAssembly();
            var asmName = asm.GetName().Name;
            var resourceRoot = $"{asmName}.TestData";
            var resource = $"{resourceRoot}.{resourceName}";
            using (var stream = asm.GetManifestResourceStream(resource))
                {
                var reader = new StreamReader(stream);
                return reader.ReadToEnd();
                }
            }
        }
    }