﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: UnitTestSensor.cs  Last modified: 2018-07-12@20:07 by Tim Long

using TA.AsteroidSurveyGenerator.Library;

namespace TA.AsteroidSurveyGenerator.Specifications.TestHelpers
    {
    class UnitTestSensor : IImagingSensor
        {
        public UnitTestSensor(int xPixels, int yPixels, double xScale, double yScale)
            {
            PixelsX = xPixels;
            PixelsY = yPixels;
            ImageScaleX = xScale;
            ImageScaleY = yScale;
            }

        public int PixelsX { get; }

        public int PixelsY { get; }

        public double ImageScaleX { get; }

        public double ImageScaleY { get; }
        }
    }