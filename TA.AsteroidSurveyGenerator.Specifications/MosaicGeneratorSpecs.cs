﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: MosaicGeneratorSpecs.cs  Last modified: 2018-08-09@23:17 by Tim Long

using System.Collections.Generic;
using System.Linq;
using Machine.Specifications;
using TA.AsteroidSurveyGenerator.Library;
using TA.AsteroidSurveyGenerator.Specifications.Contexts;

namespace TA.AsteroidSurveyGenerator.Specifications
    {
    [Subject(typeof(RowMajorMosaicGridGenerator), "Coordinate generation")]
    class when_generating_a_3_degree_strip_in_row_major_order : with_mosaic_context_builder
        {
        Establish context = () => MosaicContext = Builder
            .WithOneDegreeSquareSensor()
            .InRowMajorOrder()
            .StartAt(0.0, 0.0)
            .TerminateAt(2.0, 0)
            .Build();

        Because of = () => tiles = Generator.GenerateMosaic(StartTile, Terminator).ToList();
        It should_generate_3_tiles = () => tiles.Count.ShouldEqual(3);
        It should_generate_the_expected_centres = () => tiles.ShouldBeLike(expectedTiles);
        static readonly List<MosaicTile> expectedTiles = new List<MosaicTile>
            {
            new MosaicTile(0, 0, "1"),
            new MosaicTile(1, 0, "2"),
            new MosaicTile(2, 0, "3")
            };
        static List<MosaicTile> tiles;
        }

    [Subject(typeof(RowMajorMosaicGridGenerator), "Coordinate generation")]
    class when_generating_a_3_by_2_degree_mosaic_in_row_major_order : with_mosaic_context_builder
        {
        Establish context = () => MosaicContext = Builder
            .WithOneDegreeSquareSensor()
            .InRowMajorOrder()
            .StartAt(0.0, 0.0)
            .TerminateAt(2.0, 1.0)
            .Build();

        Because of = () => tiles = Generator.GenerateMosaic(StartTile, Terminator).ToList();
        It should_generate_6_tiles = () => tiles.Count.ShouldEqual(6);
        It should_generate_the_expected_centres = () => tiles.ShouldBeLike(expectedTiles);
        static readonly List<MosaicTile> expectedTiles = new List<MosaicTile>
            {
            new MosaicTile(0, 0, "1"),
            new MosaicTile(1, 0, "2"),
            new MosaicTile(2, 0, "3"),
            new MosaicTile(0, 1, "4"),
            new MosaicTile(1, 1, "5"),
            new MosaicTile(2, 1, "6")
            };
        static List<MosaicTile> tiles;
        }

    [Subject(typeof(RowMajorMosaicGridGenerator), "Boundary conditions")]
    class when_the_terminator_is_just_within_the_first_tile : with_mosaic_context_builder
        {
        Establish context = () => MosaicContext = Builder
            .WithOneDegreeSquareSensor()
            .InRowMajorOrder()
            .StartAt(0.0, 0.0)
            .TerminateAt(0.5, 0.5)
            .Build();

        Because of = () => tiles = Generator.GenerateMosaic(StartTile, Terminator).ToList();
        It should_generate_a_single_tile = () => tiles.Count.ShouldEqual(1);
        static List<MosaicTile> tiles;
        }

    [Subject(typeof(RowMajorMosaicGridGenerator), "Boundary conditions")]
    class when_the_terminator_is_just_inside_the_second_tile_in_each_direction : with_mosaic_context_builder
        {
        Establish context = () => MosaicContext = Builder
            .WithOneDegreeSquareSensor()
            .InRowMajorOrder()
            .StartAt(0.0, 0.0)
            .TerminateAt(0.5 + halfArcSecond, 0.5 + halfArcSecond)
            .Build();

        Because of = () => tiles = Generator.GenerateMosaic(StartTile, Terminator).ToList();
        It should_generate_4_tiles = () => tiles.Count.ShouldEqual(4);
        static List<MosaicTile> tiles;
        const double halfArcSecond = 1.0 / 60.0 / 60.0 / 2.0;
        }

    [Subject(typeof(RowMajorMosaicGridGenerator), "Overlapping tiles")]
    class when_generating_a_3_by_2_degree_grid_with_50_percent_overlap : with_mosaic_context_builder
        {
        Establish context = () => MosaicContext = Builder
            .WithOneDegreeSquareSensor()
            .InRowMajorOrder()
            .StartAt(0.0, 0.0)
            .TerminateAt(2.0, 1.0)
            .OverlapFraction(0.5)
            .Build();

        Because of = () => tiles = Generator.GenerateMosaic(StartTile, Terminator).ToList();
        It should_generate_15_tiles = () => tiles.Count.ShouldEqual(15);
        It should_generate_the_expected_centres = () => tiles.ShouldBeLike(expectedTiles);
        static readonly List<MosaicTile> expectedTiles = new List<MosaicTile>
            {
            new MosaicTile(0.0, 0, "1"),
            new MosaicTile(0.5, 0, "2"),
            new MosaicTile(1.0, 0, "3"),
            new MosaicTile(1.5, 0, "4"),
            new MosaicTile(2.0, 0, "5"),
            new MosaicTile(0.0, 0.5, "6"),
            new MosaicTile(0.5, 0.5, "7"),
            new MosaicTile(1.0, 0.5, "8"),
            new MosaicTile(1.5, 0.5, "9"),
            new MosaicTile(2.0, 0.5, "10"),
            new MosaicTile(0.0, 1.0, "11"),
            new MosaicTile(0.5, 1.0, "12"),
            new MosaicTile(1.0, 1.0, "13"),
            new MosaicTile(1.5, 1.0, "14"),
            new MosaicTile(2.0, 1.0, "15")
            };
        static List<MosaicTile> tiles;
        }
    }