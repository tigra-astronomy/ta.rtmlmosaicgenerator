﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: XmlShouldExtensions.cs  Last modified: 2018-08-08@01:37 by Tim Long

using System.Linq;
using System.Xml.Linq;
using Machine.Specifications;

namespace TA.AsteroidSurveyGenerator.Specifications.ShouldExtensions
    {
    static class XmlShouldExtensions
        {
        /// <exception cref="SpecificationException">Condition.</exception>
        public static void ShouldHaveSingleKeyNamed(this XElement element, string name)
            {
            var count = element.DescendantsAndSelf().Count(p => p.Name == name);
            if (count != 1)
                throw new SpecificationException($"Expected a single node named '{name}' but found {count}");
            }

        /// <exception cref="SpecificationException">Thrown if there is not exactly one observation.</exception>
        public static void ShouldHaveSingleObservationNamed(this XElement root, string observationname)
            {
            var observations = root.DescendantsAndSelf("Target");
            var count = observations.Count();
            if (count != 1)
                throw new SpecificationException($"Expected a single <Target> element but found {count}");
            observations.Single().Value.ShouldEqual(observationname);
            }
        }
    }