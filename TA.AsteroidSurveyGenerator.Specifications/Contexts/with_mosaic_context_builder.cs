﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: with_mosaic_context_builder.cs  Last modified: 2018-07-26@21:10 by Tim Long

using Machine.Specifications;
using TA.AsteroidSurveyGenerator.Library;
using TA.AsteroidSurveyGenerator.Specifications.Builders;

namespace TA.AsteroidSurveyGenerator.Specifications.Contexts
    {
    #region  Context base classes
    class with_mosaic_context_builder
        {
        Establish context = () => Builder = new MosaicContextBuilder();

        internal static MosaicContextBuilder Builder { get; set; }

        internal static MosaicContext MosaicContext { get; set; }

        public static IMosaicGenerator Generator => MosaicContext.Generator;

        public static IImagingSensor Sensor => MosaicContext.Sensor;

        public static IEquatorialCoordinate StartTile => MosaicContext.StartTile;

        public static IEquatorialCoordinate Terminator => MosaicContext.Terminator;
        }
    #endregion
    }