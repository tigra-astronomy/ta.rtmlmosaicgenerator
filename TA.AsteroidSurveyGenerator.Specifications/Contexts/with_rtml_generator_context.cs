﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: with_rtml_generator_context.cs  Last modified: 2018-08-08@16:52 by Tim Long

using System.Collections.Generic;
using Machine.Specifications;
using TA.AsteroidSurveyGenerator.Library;
using TA.AsteroidSurveyGenerator.Specifications.Builders;

namespace TA.AsteroidSurveyGenerator.Specifications.Contexts
    {
    #region  Context base classes
    class with_rtml_generator_context
        {
        Establish context = () => Builder = new RtmlGeneratorContextBuilder();

        protected static RtmlGeneratorContext Context { get; set; }

        protected static RtmlGeneratorContextBuilder Builder { get; private set; }

        #region Convenience properties
        public IGenerateRtml Generator => Context.Generator;

        public IEnumerable<MosaicTile> Targets => Context.Targets;
        #endregion Convenience properties
        }
    #endregion
    }