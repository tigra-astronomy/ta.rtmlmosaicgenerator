﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: SingleImageRtmlGeneratorSpecs.cs  Last modified: 2018-08-09@23:45 by Tim Long

using System.Collections.Generic;
using System.Xml.Linq;
using Machine.Specifications;
using TA.AsteroidSurveyGenerator.Library;
using TA.AsteroidSurveyGenerator.Specifications.Builders;
using TA.AsteroidSurveyGenerator.Specifications.Contexts;

namespace TA.AsteroidSurveyGenerator.Specifications
    {
    [Subject(typeof(SingleImageRtmlGenerator), "RTML generation")]
    class when_generating_rtml_for_a_single_observation_at_0_0_with_a_single_image : with_rtml_generator_context
        {
        Establish context = () => Context = Builder
            .ImageSpecification(60.0, 1, "Clear", 1, "Single unfiltered image")
            .OutputShouldBeLike("RtmlFragments.SingleObservation-SingleImage-60s-Bin1-Clear.xml")
            .RtmlGenerationStrategy(GeneratorStrategy.SingleImage)
            .Build();
        Because of = () =>
            fragment = Context.SingleImageRtmlGenerator.GenerateSingleObservation(new MosaicTile(0, 0, "1"));
        It should_generate_a_single_observation = () => fragment.ShouldBeLike(Context.ExpectedXml);
        static XElement fragment;
        }

    [Subject(typeof(SingleImageRtmlGenerator), "RTML generation")]
    class when_generating_a_single_picture_element_from_a_picture_specification : with_rtml_generator_context
        {
        Establish context = () => Context = Builder
            .ImageSpecification(60.0, 1, "Clear", 1, "Unit Test Image")
            .RtmlGenerationStrategy(GeneratorStrategy.SingleImage)
            .OutputShouldBeLike("RtmlFragments.Picture-60s-Bin1-Clear.xml")
            .Build();
        Because of = () =>
            actualElement =
                Context.SingleImageRtmlGenerator.PictureFromImageSpecification(Context.ImageSpecification);
        It should_generate_a_valid_xml_fragment = () => actualElement.ShouldBeLike(Context.ExpectedXml);
        static XElement actualElement;
        }

    [Subject(typeof(SingleImageRtmlGenerator), "RTML generation")]
    class when_generating_an_RTML_request_for_a_single_image : with_rtml_generator_context
        {
        Establish context = () => Context = Builder
            .ImageSpecification(60.0, 1, "Clear", 1, "Unit Test Image")
            .RtmlGenerationStrategy(GeneratorStrategy.SingleImage)
            .OutputShouldBeLike("RtmlFragments.FullProject-SingleObservation-SingleImage-60s-Bin1-Clear.rtml")
            .Build();
        Because of = () =>
            actualRtml =
                Context.SingleImageRtmlGenerator.GenerateRtmlProject(new List<MosaicTile>
                        {new MosaicTile(0, 0, "Test Target")});
        It should_generate_correct_rtml = () => actualRtml.ShouldBeLike(Context.ExpectedXml);
        static XElement actualRtml;
        }
    }