﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: IGenerateRtml.cs  Last modified: 2018-08-09@19:12 by Tim Long

using System.Collections.Generic;
using System.Xml.Linq;

namespace TA.AsteroidSurveyGenerator.Library
    {
    public interface IGenerateRtml
        {
        string DisplayName { get; }

        XElement GenerateRtmlProject(IEnumerable<MosaicTile> targets);
        }
    }