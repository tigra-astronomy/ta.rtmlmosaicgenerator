﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: SingleImageRtmlGenerator.cs  Last modified: 2018-08-09@23:45 by Tim Long

using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace TA.AsteroidSurveyGenerator.Library
    {
    public class SingleImageRtmlGenerator : IGenerateRtml
        {
        private readonly RtmlContact contact;
        private readonly ImageSpecification imageSpecification;
        private readonly RtmlRequest request;

        public SingleImageRtmlGenerator(ImageSpecification imageSpecification, RtmlContact contact,
            RtmlRequest request)
            {
            this.imageSpecification = imageSpecification;
            this.contact = contact;
            this.request = request;
            }

        public string DisplayName => "Single Image";

        public XElement GenerateRtmlProject(IEnumerable<MosaicTile> targets)
            {
            var requests = new List<XElement>();
            foreach (var target in targets)
                {
                var request = GenerateRequest(target);
                requests.Add(request);
                }
            var xxx = targets.Select(p => GenerateRequest(p));
            XNamespace xs = "http://www.w3.org/2001/XMLSchema-instance";
            var rtmlProject = new XElement("RTML", new XAttribute(XNamespace.Xmlns + "xs", xs),
                new XAttribute("version", "2.3"),
                new XElement("Contact",
                    new XElement("User", contact.User),
                    new XElement("Email", contact.Email),
                    new XElement("Organization", contact.Organization)
                ),
                requests
            );
            return rtmlProject;
            }

        internal XElement GenerateRequest(MosaicTile tile)
            {
            var plan = GenerateSingleObservation(tile);
            var requestElement =
                new XElement("Request", // Start of ACPS Plan, the minimum schedulable unit
                    new XElement("ID", request.PlanName),
                    new XElement("UserName", contact.User),
                    new XElement("Project", request.Project),
                    plan,
                    new XElement("Correction",
                        new XAttribute("dark", request.CalibrateImages),
                        new XAttribute("flat", request.CalibrateImages),
                        new XAttribute("zero", request.CalibrateImages)
                    )
                );
            return requestElement;
            }

        internal XElement GenerateSingleObservation(MosaicTile tile)
            {
            var imageElement = PictureFromImageSpecification(imageSpecification);
            var targetElement = new XElement("Target",
                new XElement("Name", tile.TileName),
                new XElement("Coordinates",
                    new XElement("RightAscension", tile.RightAscension.Value),
                    new XElement("Declination", tile.Declination.Value)
                ),
                imageElement
            );
            return targetElement;
            }

        internal XElement PictureFromImageSpecification(ImageSpecification imageSpec)
            {
            var picture = new XElement("Picture", new XAttribute("count", imageSpec.Count),
                new XElement("Name", imageSpec.Name),
                new XElement("Description", imageSpec.Description),
                new XElement("ExposureTime", imageSpec.Exposure),
                new XElement("Binning", imageSpec.Binning),
                new XElement("Filter", imageSpec.FilterName)
            );
            return picture;
            }
        }
    }