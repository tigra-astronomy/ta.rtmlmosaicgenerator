﻿namespace TA.AsteroidSurveyGenerator.Library {
    public class RtmlContact
        {
        public string User { get; set; }

        public string Email { get; set; }

        public string Organization { get; set; }
        }
    }