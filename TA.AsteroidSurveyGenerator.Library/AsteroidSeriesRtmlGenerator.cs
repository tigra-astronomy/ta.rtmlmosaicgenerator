﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: AsteroidSeriesRtmlGenerator.cs  Last modified: 2018-08-09@19:12 by Tim Long

using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace TA.AsteroidSurveyGenerator.Library
    {
    public class AsteroidSeriesRtmlGenerator : IGenerateRtml
        {
        public string DisplayName => "Asteroid Follow-up Series";

        public XElement GenerateRtmlProject(IEnumerable<MosaicTile> targets)
            {
            throw new NotImplementedException();
            }

        public string Generate()
            {
            throw new NotImplementedException();
            }

        internal IEnumerable<XElement> GenerateObservationSeries(ObservationSeriesSpecification series,
            MosaicTile position, ImageSpecification exposure)
            {
            var observations = new List<XElement>();
            // ToDo
            return observations;
            }

        internal XElement GenerateSingleObservation(string name, MosaicTile position, ImageSpecification exposure)
            {
            var imageElement = PictureFromImageSpecification(exposure);
            var targetElement = new XElement("Target",
                new XElement("Name", name),
                new XElement("Coordinates",
                    new XElement("RightAscension", position.RightAscension.Value),
                    new XElement("Declination", position.Declination.Value)
                ),
                imageElement
            );
            return targetElement;
            }

        internal XElement PictureFromImageSpecification(ImageSpecification imageSpec)
            {
            var picture = new XElement("Picture", new XAttribute("count", imageSpec.Count),
                new XElement("Name", imageSpec.Name),
                new XElement("Description", imageSpec.Description),
                new XElement("ExposureTime", imageSpec.Exposure),
                new XElement("Binning", imageSpec.Binning),
                new XElement("Filter", imageSpec.FilterName)
            );
            return picture;
            }
        }
    }