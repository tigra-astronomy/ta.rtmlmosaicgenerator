﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: ObservationSeriesSpecification.cs  Last modified: 2018-08-09@18:02 by Tim Long

using System;

namespace TA.AsteroidSurveyGenerator.Library
    {
    /// <summary>
    ///     Specifies a time-spaced series of observations of a single target.
    /// </summary>
    internal class ObservationSeriesSpecification
        {
        public TimeSpan ObservationInterval { get; set; }

        public TimeSpan ObservationIntervalTolerance { get; set; }

        public int SeriesLength { get; set; }
        }
    }