﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: IImagingSensor.cs  Last modified: 2018-07-12@20:07 by Tim Long

namespace TA.AsteroidSurveyGenerator.Library
    {
    public interface IImagingSensor
        {
        /// <summary>
        ///     Number of pixels in the X axis (usually the number of CCD columns)
        /// </summary>
        int PixelsX { get; }

        /// <summary>
        ///     Number of pixels in the Y axis (usually CCD rows)
        /// </summary>
        int PixelsY { get; }

        /// <summary>
        ///     Image scale in arc seconds per pixel in the X axis
        /// </summary>
        double ImageScaleX { get; }

        /// <summary>
        ///     Image scale in arc seconds per pixel in the Y axis
        /// </summary>
        double ImageScaleY { get; }
        }
    }