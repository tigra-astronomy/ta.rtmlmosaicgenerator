﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: RtmlRequest.cs  Last modified: 2018-08-09@23:45 by Tim Long

namespace TA.AsteroidSurveyGenerator.Library
    {
    /// <summary>
    ///     Details of an RTML request, which map to the ACP Scheduler Project
    /// </summary>
    public class RtmlRequest
        {
        public string Project { get; set; }

        public string PlanName { get; set; }

        public bool CalibrateImages { get; set; }
        }
    }