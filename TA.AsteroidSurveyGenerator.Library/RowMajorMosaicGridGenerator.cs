﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: RowMajorMosaicGridGenerator.cs  Last modified: 2018-07-12@20:06 by Tim Long

using System.Collections.Generic;
using System.Diagnostics.Contracts;
using TA.ObjectOrientedAstronomy.FundamentalTypes;

namespace TA.AsteroidSurveyGenerator.Library
    {
    public class RowMajorMosaicGridGenerator : IMosaicGenerator
        {
        private readonly double halfSensorHeight;
        private readonly double halfSensorWidth;
        private readonly IImagingSensor imagingSensor;
        private readonly double overlap;
        private readonly double xIncrement;
        private readonly double yIncrement;
        private MosaicTile currentTile;

        public RowMajorMosaicGridGenerator(IImagingSensor imagingSensor, double overlap = 0.0)
            {
            Contract.Requires(overlap >= 0.0 && overlap < 1.0);
            Contract.Requires(imagingSensor != null);
            this.imagingSensor = imagingSensor;
            this.overlap = overlap;
            xIncrement = imagingSensor.PixelsX * imagingSensor.ImageScaleX / 3600.0;
            yIncrement = imagingSensor.PixelsY * imagingSensor.ImageScaleY / 3600.0;
            halfSensorWidth = xIncrement / 2.0;
            halfSensorHeight = yIncrement / 2.0;
            }

        /// <inheritdoc cref="IMosaicGenerator" />
        public IEnumerable<MosaicTile> GenerateMosaic(IEquatorialCoordinate start, IEquatorialCoordinate terminator)
            {
            currentTile = new MosaicTile(start);
            int sequenceNumber = 0;
            var xLimit = terminator.RightAscension.Value + halfSensorWidth;
            var yLimit = terminator.Declination.Value + halfSensorHeight;
            do // Outer loop - varies the Row number, i.e. increases declination
                {
                do // Inner loop varies the column i.e. increases Right Ascension
                    {
                    ++sequenceNumber;
                    yield return new MosaicTile(currentTile, sequenceNumber.ToString());
                    var nextX = currentTile.RightAscension.Value + xIncrement * (1.0 - overlap);
                    currentTile.RightAscension = new RightAscension(nextX);
                    } while (currentTile.RightAscension.Value < xLimit);
                var nextY = currentTile.Declination.Value + yIncrement * (1.0 - overlap);
                currentTile.Declination = new Declination(nextY);
                currentTile.RightAscension = start.RightAscension;
                } while (currentTile.Declination.Value < yLimit);
            }

        private bool CoordinateIsWithinSensor(IEquatorialCoordinate testCoordinate)
            {
            var xMin = currentTile.RightAscension.Value - halfSensorWidth;
            var xMax = currentTile.RightAscension.Value + halfSensorWidth;
            var yMin = currentTile.Declination.Value - halfSensorHeight;
            var yMax = currentTile.Declination.Value + halfSensorHeight;
            var hourAngle = testCoordinate.RightAscension.Value;
            var withinX = hourAngle >= xMin && hourAngle <= xMax;
            var declination = testCoordinate.Declination.Value;
            var withinY = declination >= yMin && declination <= yMax;
            return withinX && withinY;
            }
        }
    }