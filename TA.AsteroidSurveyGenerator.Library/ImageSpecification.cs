﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: ImageSpecification.cs  Last modified: 2018-08-08@18:28 by Tim Long

using JetBrains.Annotations;

namespace TA.AsteroidSurveyGenerator.Library
    {
    /// <summary>
    ///     Describes the exposure settings for an image
    /// </summary>
    public class ImageSpecification
        {
        /// <summary>
        ///     The image name must uniquely identify the image from other images in an image set.
        /// </summary>
        [NotNull]
        public string Name { get; set; }

        public double Exposure { get; set; }

        public int Binning { get; set; }

        [NotNull]
        public string FilterName { get; set; }

        /// <summary>
        ///     Human readable description of the image set (can be empty but not null)
        /// </summary>
        [NotNull]
        public string Description { get; set; }

        public int Count { get; set; }
        }
    }