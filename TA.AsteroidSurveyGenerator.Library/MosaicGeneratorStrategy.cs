﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: MosaicGeneratorStrategy.cs  Last modified: 2018-07-06@23:42 by Tim Long

namespace TA.AsteroidSurveyGenerator.Library
    {
    public enum MosaicGeneratorStrategy
        {
        RowMajor,
        ColumnMajor,
        Spiral,
        Box
        }
    }