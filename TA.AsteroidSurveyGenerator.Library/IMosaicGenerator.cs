﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: IMosaicGenerator.cs  Last modified: 2018-07-12@20:07 by Tim Long

using System.Collections.Generic;

namespace TA.AsteroidSurveyGenerator.Library
    {
    public interface IMosaicGenerator
        {
        /// <summary>
        ///     Generates a sequence of mosaic tiles, starting at the <paramref name="start" /> coordinate
        ///     and finishing when a tile encloses the <paramref name="terminator" /> coordinate.
        /// </summary>
        /// <param name="start">The centre point of the first tile.</param>
        /// <param name="terminator">
        ///     The coordinate which, when enclosed by a generated tile, terminates the sequence.
        /// </param>
        /// <returns>An enumerable sequence of <see cref="MosaicTile" />.</returns>
        IEnumerable<MosaicTile> GenerateMosaic(IEquatorialCoordinate start, IEquatorialCoordinate terminator);
        }
    }