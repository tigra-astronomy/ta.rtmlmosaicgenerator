﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: MosaicTile.cs  Last modified: 2018-08-09@23:25 by Tim Long

using TA.ObjectOrientedAstronomy.FundamentalTypes;

namespace TA.AsteroidSurveyGenerator.Library
    {
    public class MosaicTile : IEquatorialCoordinate
        {
        private const string Anonymous = "unnamed";

        public MosaicTile(double ra, double dec, string name = Anonymous) : this(new RightAscension(ra),
            new Declination(dec), name) { }

        public MosaicTile(IEquatorialCoordinate location, string name = Anonymous) : this(location.RightAscension,
            location.Declination, name) { }

        public MosaicTile(RightAscension rightAscension, Declination declination, string name = Anonymous)
            {
            RightAscension = rightAscension;
            Declination = declination;
            TileName = name;
            }

        public string TileName { get; set; }

        public RightAscension RightAscension { get; set; }

        public Declination Declination { get; set; }

        public override string ToString()
            {
            return $"{nameof(RightAscension)}: {RightAscension}, {nameof(Declination)}: {Declination}";
            }
        }
    }