﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: AssemblyScopedAttributes.cs  Last modified: 2018-08-08@01:37 by Tim Long

using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("TA.AsteroidSurveyGenerator.Specifications")]