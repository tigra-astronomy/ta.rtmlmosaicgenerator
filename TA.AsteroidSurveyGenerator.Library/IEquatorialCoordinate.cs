﻿// This file is part of the TA.AsteroidSurveyGenerator project
// 
// Copyright © 2016-2018 Tigra Astronomy, all rights reserved.
// 
// File: IEquatorialCoordinate.cs  Last modified: 2018-07-12@19:59 by Tim Long

using TA.ObjectOrientedAstronomy.FundamentalTypes;

namespace TA.AsteroidSurveyGenerator.Library
    {
    public interface IEquatorialCoordinate
        {
        RightAscension RightAscension { get; }

        Declination Declination { get; }
        }
    }